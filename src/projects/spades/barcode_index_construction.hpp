#pragma once

#include "common/barcode_index/barcode_index_builder.hpp"
#include "utils/logger/logger.hpp"
#include "common/pipeline/stage.hpp"

using namespace barcode_index;

namespace debruijn_graph {
    class BarcodeMapConstructionStage : public spades::AssemblyStage {

    public:

        BarcodeMapConstructionStage() :
                AssemblyStage("Barcode map construction", "barcode_map_construction") {
        }

        void run(debruijn_graph::conj_graph_pack &graph_pack, const char *);
        DECL_LOGGER("BarcodeMapConstrusctionStage")
    };

    void taxatree_to_edges_from_file(string filename,
                                     barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph> &barcodeindex);

    std::vector<std::vector<TaxId>> lca_preprocessing(const std::vector<string>& taxatree_str_vec,
                                                      std::vector<size_t>& count_vec, size_t& null_counts, size_t& total_counts, size_t& longest_lineage,
                                                      const FrameBarcodeIndexInfoExtractor& extractor);

    TaxId fuzzy_lca(const std::vector<string>& taxatree_str_vec, std::vector<size_t> count_vec,
                    const FrameBarcodeIndexInfoExtractor& extractor, const double& max_drilldown_ratio=0.85,
                    const double& max_mismatch_ratio=0.05, double minimum_assign_ratio=0.2);

    TaxId majority_vote_lca(std::vector<string>& taxatree_str_vec, std::vector<size_t>& count_vec,
                            const FrameBarcodeIndexInfoExtractor& extractor, double maj_frac=0.5);

    TaxId last_common_ancestor(std::vector<string> &taxatree_vec, std::vector<size_t> &count_vec,
                               const FrameBarcodeIndexInfoExtractor& extractor);

    std::string lca_taxatree_from_map(std::unordered_map<std::string, size_t> &taxatree_map,
                               const FrameBarcodeIndexInfoExtractor &extractor);

    void fill_taxatree_and_count_vectors_from_edge(const FrameBarcodeIndexInfoExtractor& extractor,
                                                   std::vector<string>& taxatree_vector,
                                                   std::vector<size_t>& count_vector, EdgeId edge);

    void test_various_lca_settings(barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph>& barcodeindex,
                                   const FrameBarcodeIndexInfoExtractor& extractor, const string& file_name);

    void write_contigs_to_fasta(const barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph>& barcodeindex,
                                const FrameBarcodeIndexInfoExtractor& extractor, const string& file_name);

    void assign_taxonomy_to_edges(barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph>& barcodeindex,
                                  const FrameBarcodeIndexInfoExtractor& extractor);

    std::string taxatree_from_lca_and_vec(const std::vector<string> &taxatree_vector, const TaxId &lca);

    void assure_lca_taxatree_in_taxatree_codes(const std::vector<string> &taxatree_vector, const TaxId &lca,
                                               barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph> &barcodeindex);

} //debruijn_graph
