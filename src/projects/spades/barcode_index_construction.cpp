#include "barcode_index_construction.hpp"

#include "common/barcode_index/barcode_info_extractor.hpp"
#include "common/modules/path_extend/read_cloud_path_extend/fragment_statistics/distribution_extractor_helper.hpp"

#include <vector>
#include <iomanip>

namespace debruijn_graph {
    //todo remove from here!

    bool HasReadClouds(config::dataset ds) {
        bool has_read_clouds = false;
        for (const auto& lib: ds.reads) {
            if (lib.type() == io::LibraryType::Clouds10x) {
                has_read_clouds = true;
            }
        }
        return has_read_clouds;
    }

    //TODO: remove, not used anywhere. Maybe someday in the future
    void taxatree_to_edges_from_file(std::string filename,
                                     barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph> &barcodeindex) {
        std::ifstream in_lca_file(filename);
        std::string line;
        while(std::getline(in_lca_file, line)){
            //edge_1, edge_2, taxatree = parsed_info
        }
    }

    std::vector<std::vector<TaxId>>
    lca_preprocessing(const std::vector<string> &taxatree_str_vec, std::vector<size_t> &count_vec, size_t &null_counts,
                      size_t &total_counts, size_t &longest_lineage, const FrameBarcodeIndexInfoExtractor &extractor) {
        std::vector<std::vector<TaxId>> taxatree_vec;
        for ( const std::string& taxatree_str : taxatree_str_vec ) {
            if (taxatree_str != "0") {
                std::vector<TaxId> taxid_vec = extractor.ToTaxaTreeVector(taxatree_str, '.');
                taxatree_vec.push_back(taxid_vec);
                longest_lineage = std::max(taxid_vec.size(), longest_lineage);
            }
            else {
                // remove taxa 0 but add counts to total_counts
                auto it = std::find(taxatree_str_vec.begin(), taxatree_str_vec.end(), taxatree_str);
                auto no_taxa_index = std::distance(taxatree_str_vec.begin(), it);
                null_counts += count_vec[no_taxa_index];
                count_vec.erase(count_vec.begin() + no_taxa_index);
            }
        }
        for (const size_t& count : count_vec) {
            total_counts += count;
        }
        return taxatree_vec;
    }

    TaxId fuzzy_lca(const std::vector<string> &taxatree_str_vec, std::vector<size_t> count_vec,
                    const FrameBarcodeIndexInfoExtractor &extractor, const double &max_drilldown_ratio,
                    const double &max_mismatch_ratio, double minimum_assign_ratio) {
        //max_drilldown_ratio=ratio of non-0 taxa to allow at a deeper taxid in same lineage
        //max_mismatch_ratio=ratio of non-0 taxa to allow as a mismatch

        size_t null_counts = 0;
        size_t total_counts = 0;
        size_t longest_lineage = 0;
        // transfer taxatree_strings to taxid_vectors, remove 0 taxids in count_vec, place counts in null_counts,
        // get total_counts & longest_lineage.
        std::vector<std::vector<TaxId>> taxatree_vec = lca_preprocessing(taxatree_str_vec, count_vec, null_counts,
                                                                         total_counts, longest_lineage, extractor);

        TaxId lca = 0;
        VERIFY_MSG(taxatree_vec.size() == count_vec.size(), "ERROR: taxatree_vec.size is not count_vec.size in max_lca");
        if (!count_vec.empty()) { //if false only taxa 0 was part of taxatree_str_vec.
            size_t mismatches = 0;
            size_t drilldowns = 0;
            if (total_counts > (minimum_assign_ratio * (total_counts + null_counts))) { //minimum 20% assigned taxids.
                size_t i = 0;
                TaxId proposed_lca = 0;
                size_t prop_lca_counts = total_counts;
                while (i < longest_lineage && mismatches <= (max_mismatch_ratio * total_counts)
                       && drilldowns <= (max_drilldown_ratio * total_counts)) {
                    prop_lca_counts = 0;
                    auto most_common_index = std::distance(count_vec.begin(), std::max_element(count_vec.begin(),
                            count_vec.end())); // find index of taxid with highest count
                    if (i < taxatree_vec[most_common_index].size()){
                        proposed_lca = taxatree_vec[most_common_index][i];
                    } else {
                        size_t max_sub_count = 0;
                        for ( auto taxatree : taxatree_vec ) {
                            if ( i < taxatree.size() && count_vec[i] > max_sub_count ) {
                                //get most common taxa as proposed_lca within taxatree that aren't too long.
                                proposed_lca = taxatree[i];
                                max_sub_count = count_vec[i];
                            }
                        }
                    }
                    size_t j = 0;
                    TRACE("i=" << i << " Taxatree_vec: " << taxatree_vec << " Count_vec:" << count_vec);
                    while ( j < count_vec.size() ) {
                        TRACE("j=" << j << " i=" << i);
                        auto taxatree = taxatree_vec[j];
                        if (i >= taxatree.size()) {//first time no match: erase
                            VERIFY_MSG(i == taxatree.size(), "fuzzy_lca drilldown: i should never exceed taxatree_size");
                            TRACE("Drilldown detected: " << taxatree);
                            drilldowns += count_vec[j];
                            taxatree_vec.erase(taxatree_vec.begin()+j);
                            count_vec.erase(count_vec.begin()+j);
                            continue; // do not increment j.
                        }
                        TRACE("No drilldowns");
                        if (taxatree[i] != proposed_lca) {
                            mismatches += count_vec[j];
                            taxatree_vec.erase(taxatree_vec.begin()+j);
                            count_vec.erase(count_vec.begin()+j);
                            continue; // do not increment j.
                        }
                        VERIFY_MSG(i < taxatree.size() && taxatree[i] == proposed_lca,
                                   "fuzzy_lca: not drilldown or mismatch but still illegal. i: " << i << ", taxatree: " << taxatree);
                        prop_lca_counts += count_vec[j];
                        TRACE("@j=" << j << " Taxatree_vec: " << taxatree_vec << " Count_vec:" << count_vec);
                        ++j;
                    }
                    //update lca
                    if (mismatches <= (max_mismatch_ratio * total_counts)
                        && drilldowns <= (max_drilldown_ratio * total_counts)) {
                        lca = proposed_lca;
                    }
                    ++i;
                }
            }
        }
        return lca;
    }

    TaxId majority_vote_lca(std::vector<string> &taxatree_str_vec, std::vector<size_t> &count_vec,
                            const FrameBarcodeIndexInfoExtractor &extractor, double maj_frac) {
        size_t null_counts = 0;
        size_t total_counts = 0;
        size_t longest_lineage = 0;
        // transfer taxatree_strings to taxid_vectors, remove 0 taxids in count_vec, place counts in null_counts,
        // get total_counts & longest_lineage.
        std::vector<std::vector<TaxId>> taxatree_vec = lca_preprocessing(taxatree_str_vec, count_vec, null_counts,
                                                                         total_counts, longest_lineage, extractor);

        size_t min_majority = total_counts * maj_frac;
        // find index of taxid with highest count
        auto most_common_index = std::distance(count_vec.begin(),
                                               std::max_element(count_vec.begin(), count_vec.end()));

        TaxId lca = 0;

        VERIFY_MSG(taxatree_vec.size() == count_vec.size(), "ERROR: taxatree_vec.size is not count_vec.size in max_lca");
        if (!count_vec.empty()) { //in this case only taxa 0 was part of taxatree_str_vec.
            if (total_counts > (0.20 * (total_counts + null_counts))) { //minimum 20% assigned taxids.
                TRACE("In main body fuzzy_lca");
                size_t i = 0;
                TaxId proposed_lca = 0;
                size_t prop_lca_counts = total_counts;
                while (i < longest_lineage && prop_lca_counts >= min_majority) {
                    prop_lca_counts = 0;
                    if (i < taxatree_vec[most_common_index].size()) {
                        proposed_lca = taxatree_vec[most_common_index][i];
                    } else {
                        size_t max_sub_count = 0;
                        for (auto taxatree : taxatree_vec) {
                            if (i < taxatree.size() && count_vec[i] > max_sub_count) {
                                //get most common taxa as proposed_lca within taxatree that aren't too long.
                                proposed_lca = taxatree[i];
                                max_sub_count = count_vec[i];
                            }
                        }
                    }
                    TRACE("iteration=" << i << " proposed_lca=" << proposed_lca);
                    size_t count_vec_pos = 0;
                    for (auto taxatree : taxatree_vec) {
                        if (i < taxatree.size() && taxatree[i] == proposed_lca) {
                            prop_lca_counts += count_vec[count_vec_pos];
                        }
                        ++count_vec_pos;
                    }
                    if (prop_lca_counts >= min_majority) {
                        lca = proposed_lca;
                    }
                    ++i;
                    TRACE("Done iteration: " << i << " lca: " << lca);
                }
            }
            return lca;
        } else return lca;
    }

    TaxId last_common_ancestor(std::vector<string> &taxatree_vec, std::vector<size_t> &count_vec,
                               const FrameBarcodeIndexInfoExtractor &extractor) {
        VERIFY_MSG(taxatree_vec.size() == count_vec.size(), "ERROR: taxatree_vec.size is not count_vec.size before lca");
        // wrapper to change underlying lca_algorithm.
        TaxId lca = fuzzy_lca(taxatree_vec, count_vec, extractor, 0.85, 0.07);
        return lca;
    }

    std::string lca_taxatree_from_map(std::unordered_map<std::string, size_t> &taxatree_map,
                                               const FrameBarcodeIndexInfoExtractor &extractor) {
        // wrapper to change underlying lca_algorithm.
        std::vector<string> taxatree_vec;
        std::vector<size_t> count_vec;
        for (auto it = taxatree_map.begin(); it != taxatree_map.end(); it++) {
            taxatree_vec.push_back(it->first);
            count_vec.push_back(it->second);
        }
        TaxId lca = fuzzy_lca(taxatree_vec, count_vec, extractor, 0.4, 0.03, 0.3);
        std::string taxatree = taxatree_from_lca_and_vec(taxatree_vec, lca);

        return taxatree;
    }

    void fill_taxatree_and_count_vectors_from_edge(const FrameBarcodeIndexInfoExtractor &extractor,
                                                   std::vector<string> &taxatree_vector,
                                                   std::vector<size_t> &count_vector, EdgeId edge) {
        TaxId taxid;
        std::string taxatree;
        size_t count;
        typename FrameBarcodeIndexInfoExtractor::taxid_distribution_t::const_iterator taxid_it;
        for (taxid_it = extractor.taxid_iterator_begin(edge); taxid_it != extractor.taxid_iterator_end(edge);
             taxid_it++ ) {
            taxid = taxid_it->first;
            taxatree = extractor.TaxaTreeFromTaxId(taxid);
            if (taxatree.find("SUBSTR") != std::string::npos) {
                INFO("SUBSTR found in fill: " << taxatree);
            }
            auto taxatree_match = find(taxatree_vector.begin(), taxatree_vector.end(), taxatree);
            if (taxatree_match == taxatree_vector.end()) {
                taxatree_vector.push_back(taxatree);
                count = extractor.GetTaxidCount(edge, taxid);
                count_vector.push_back(count);
            }
            else {
                size_t match_index = std::distance(taxatree_vector.begin(), taxatree_match);
                count_vector[match_index] += extractor.GetTaxidCount(edge, taxid);
            }
        }
    }

    std::string taxatree_from_lca_and_vec(const std::vector<string> &taxatree_vector, const TaxId &lca) {
        //Get taxatree for LCA
        string lca_taxatree;
        string lca_str = std::to_string(lca);
        size_t lca_pos;
        if (lca_str == "0") return lca_str;

        for (const string& taxatree : taxatree_vector) {
            lca_pos = taxatree.find(lca_str);
            if (lca_pos != string::npos){
                lca_taxatree = taxatree.substr(0, lca_pos + lca_str.size());
                break;
            }
        }
        return lca_taxatree;
    }

    void assure_lca_taxatree_in_taxatree_codes(const std::vector<string> &taxatree_vector, const TaxId &lca,
                                               barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph> &barcodeindex) {
        //Get taxatree for LCA
        string lca_taxatree = taxatree_from_lca_and_vec(taxatree_vector, lca);
        if (lca_taxatree == "0") return;

        if (lca_taxatree.find("SUBSTR") != std::string::npos) {
            INFO("SUBSTR found in assure: " << lca_taxatree << " lca_taxatree: " << lca_taxatree);
        }

        //INFO("Inserting taxatree: " << lca_taxatree << " lca_pos: " << lca_pos << " size: " << lca_str.size());
        //Insert lca_taxatree into taxatree_codes_ otherwise
        barcodeindex.taxatree_codes_.AddTaxaTree(lca_taxatree);
    }

    void assign_taxonomy_to_edges(barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph> &barcodeindex,
                                  const FrameBarcodeIndexInfoExtractor &extractor) {
        std::unordered_set<EdgeId> completed_edges;
        for ( auto &p : barcodeindex.edge_to_entry_ ) {
            EdgeId edge = p.first; // edgeID p.first; edgeEntry p.second
            EdgeId conjugate_edge = extractor.GetConjugateEdge(edge);
            if (completed_edges.find(edge) != completed_edges.end()) {
                VERIFY_MSG(completed_edges.find(conjugate_edge) != completed_edges.end(),
                           "Conjugate edge should be in completed_edges along with original edge.");
                continue;
            }

            std::vector<string> taxatree_vector;
            std::vector<size_t> count_vector;
            fill_taxatree_and_count_vectors_from_edge(extractor, taxatree_vector, count_vector, edge);
            fill_taxatree_and_count_vectors_from_edge(extractor, taxatree_vector, count_vector, conjugate_edge);
            if (taxatree_vector.empty() && count_vector.empty()) {
                taxatree_vector.push_back("0");
                count_vector.push_back(1);
            }

            DEBUG("EdgeId: " << edge << " & " << conjugate_edge << " Approximate_size: "
                             << (p.second.GetFrameSize() * p.second.GetNumberOfFrames()) );
            for (size_t i = 0; i != taxatree_vector.size(); i++ ){
                DEBUG("TaxaTree: " << taxatree_vector[i] << ", Count: " << count_vector[i]);
            }

            // Beware that lca function can mess with count_vector length so just use once.
            TaxId lca = last_common_ancestor(taxatree_vector, count_vector, extractor);
            DEBUG("LCA: " << lca);

            assure_lca_taxatree_in_taxatree_codes(taxatree_vector, lca, barcodeindex);

            p.second.SetTaxonomy(lca);
            barcodeindex.edge_to_entry_.at(conjugate_edge).SetTaxonomy(lca);
            completed_edges.insert(edge);
            completed_edges.insert(conjugate_edge);
        }
    }

    void test_various_lca_settings(barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph> &barcodeindex,
                                   const FrameBarcodeIndexInfoExtractor &extractor, const string &file_name) {
        std::ofstream out_lca_file;
        string out_location = fs::append_path(cfg::get().output_dir, file_name);
        out_lca_file.open (out_location);
        std::unordered_set<EdgeId> completed_edges;
        for ( auto &p : barcodeindex.edge_to_entry_ ) {
            EdgeId edge = p.first; // edgeID p.first; edgeEntry p.second
            EdgeId conjugate_edge = extractor.GetConjugateEdge(edge);
            if (completed_edges.find(edge) != completed_edges.end()) {
                VERIFY_MSG(completed_edges.find(conjugate_edge) != completed_edges.end(),
                           "Conjugate edge should be in completed_edges along with original edge.");
                continue;
            }
            string seq = extractor.GetSequence(edge);
            std::vector<string> taxatree_vector;
            std::vector<size_t> count_vector;
            fill_taxatree_and_count_vectors_from_edge(extractor, taxatree_vector, count_vector, edge);
            fill_taxatree_and_count_vectors_from_edge(extractor, taxatree_vector, count_vector, conjugate_edge);
            if (taxatree_vector.empty() && count_vector.empty()) {
                taxatree_vector.push_back("0");
                count_vector.push_back(1);
            }
            out_lca_file << ">EdgeID=" << edge << "," << conjugate_edge << " Length=" << seq.length();
            for (double maj_frac = 0.05; maj_frac < 0.95; maj_frac += 0.05){
                std::vector<size_t> count_vector_copy(count_vector);
                TaxId lca = majority_vote_lca(taxatree_vector, count_vector_copy, extractor, maj_frac);
                assure_lca_taxatree_in_taxatree_codes(taxatree_vector, lca, barcodeindex);
                string taxatree = extractor.TaxaTreeFromTaxId(lca);
                out_lca_file << std::setprecision(2) << " Maj_vote_LCA_" << maj_frac << "=" << taxatree;
            }
            for (double drilldown_thresh = 0.05; drilldown_thresh < 0.9; drilldown_thresh += 0.05 ) {
                for (double mismatch_thresh = 0.05; mismatch_thresh < 0.8; mismatch_thresh += 0.05 ) {
                    std::vector<size_t> count_vector_copy(count_vector);
                    TaxId lca = fuzzy_lca(taxatree_vector, count_vector_copy, extractor, drilldown_thresh, mismatch_thresh);
                    assure_lca_taxatree_in_taxatree_codes(taxatree_vector, lca, barcodeindex);
                    string taxatree = extractor.TaxaTreeFromTaxId(lca);
                    out_lca_file << std::setprecision(2) << " Fuzzy_LCA_drilldown_" << drilldown_thresh << "_mismatch_"
                                 << mismatch_thresh << "=" << taxatree;
                }
            }
            out_lca_file << std::endl;
            out_lca_file << seq << std::endl;

            completed_edges.insert(edge);
            completed_edges.insert(conjugate_edge);
        }
    }

    void write_contigs_to_fasta(const barcode_index::FrameBarcodeIndex<debruijn_graph::DeBruijnGraph> &barcodeindex,
                                const FrameBarcodeIndexInfoExtractor &extractor, const string &file_name) {
        std::ofstream fasta_taxa_file;
        string fasta_location = fs::append_path(cfg::get().output_dir, file_name);
        fasta_taxa_file.open (fasta_location);
        std::unordered_set<EdgeId> completed_edges;
        for ( auto &p : barcodeindex.edge_to_entry_ ) {
            EdgeId edge = p.first; // edgeID p.first; edgeEntry p.second
            EdgeId conjugate_edge = extractor.GetConjugateEdge(edge);
            if (completed_edges.find(edge) != completed_edges.end()) {
                VERIFY_MSG(completed_edges.find(conjugate_edge) != completed_edges.end(),
                           "Conjugate edge should be in completed_edges along with original edge.");
                continue;
            }
            string seq = extractor.GetSequence(edge);
            string taxatree = extractor.GetTaxaTreeFromEdge(edge);
            fasta_taxa_file << ">EdgeID=" << edge << "," << conjugate_edge << " Length=" << seq.length() <<
                " Taxatree=" << taxatree << std::endl;
            fasta_taxa_file << seq << std::endl;
            completed_edges.insert(edge);
            completed_edges.insert(conjugate_edge);
        }
    }

    void BarcodeMapConstructionStage::run(debruijn_graph::conj_graph_pack &graph_pack, const char *) {
        using path_extend::read_cloud::fragment_statistics::ClusterStatisticsExtractorHelper;

        INFO("Barcode index construction started...");
        const auto& dataset_info = cfg::get().ds;
        if (not HasReadClouds(dataset_info)) {
            INFO("Read cloud libraries have not been found. Skipping barcode index construction.")
            return;
        }
        size_t num_threads = cfg::get().max_threads;
        for (size_t i = 0; i < cfg::get().ds.reads.lib_count(); ++i) {
            auto &lib = cfg::get_writable().ds.reads[i];
            if (lib.type() == io::LibraryType::Clouds10x) {
                graph_pack.EnsureIndex();
                graph_pack.EnsureBasicMapping();
                std::vector <io::SingleStreamPtr> reads;
                for (const auto &read: lib.reads()) {
                    auto stream = io::EasyStream(read, false);
                    reads.push_back(stream);
                }
                FrameMapperBuilder<Graph> mapper_builder(graph_pack.barcode_mapper,
                                                         cfg::get().pe_params.read_cloud.edge_tail_len,
                                                         cfg::get().pe_params.read_cloud.frame_size);
                mapper_builder.FillMap(reads, graph_pack.index, graph_pack.kmer_mapper);
                INFO("Barcode index construction finished.");
                FrameBarcodeIndexInfoExtractor extractor(graph_pack.barcode_mapper, graph_pack.g);
                if (cfg::get().taxonomy) {
                    INFO("Starting taxonomic assignment to all edges.")
                    assign_taxonomy_to_edges(graph_pack.barcode_mapper, extractor);
                    INFO("Taxonomy assigned to all edges.");
                    //test_various_lca_settings(graph_pack.barcode_mapper, extractor, "taxonomy_edges_tests.fasta");
                    write_contigs_to_fasta(graph_pack.barcode_mapper, extractor, "taxonomy_of_edges.fasta");
                    INFO("Wrote taxonomy_of_edges.fasta in Kmer dir.")
                }
                size_t length_threshold = cfg::get().pe_params.read_cloud.long_edge_length_lower_bound;
                INFO("Average barcode coverage: " + std::to_string(extractor.AverageBarcodeCoverage(length_threshold)));
                ClusterStatisticsExtractorHelper cluster_extractor_helper(graph_pack.g, graph_pack.barcode_mapper,
                                                                          cfg::get().pe_params.read_cloud, num_threads);
                auto cluster_statistics_extractor = cluster_extractor_helper.GetStatisticsExtractor();
                auto distribution_pack = cluster_statistics_extractor.GetDistributionPack();
                lib.data().read_cloud_info.fragment_length_distribution = distribution_pack.length_distribution_;
            }
        }
//        graph_pack.read_cloud_distribution_pack = distribution_pack;
    }

}