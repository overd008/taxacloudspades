#!/bin/bash
# Author: Sam Overduin
spades_py_executable="/mnt/LTR_userdata/overd008/tools/bin/spades.py"

spades_folder="$1"

#cd "$spades_folder"
if [ "$2" == "barcode" ]; then
    rm -r "$spades_folder"/K55/saves/barcode_map_construction
    echo "distance_estimation" > "$spades_folder"/K55/saves/checkpoint.dat
    rm -r "$spades_folder/.bin_reads"
elif [ "$2" == "repeat" ]; then
    echo "barcode_map_construction" > "$spades_folder"/K55/saves/checkpoint.dat
else
    echo "$2 not found. Usage: reset_spades_for_taxa.sh spades-folder [barcode|repeat] ."
fi

rm "$spades_folder"/pipeline_state/stage_3_k55
rm "$spades_folder"/pipeline_state/stage_4_copy_files
rm "$spades_folder"/pipeline_state/stage_5_as_finish
rm "$spades_folder"/pipeline_state/stage_6_bs
rm "$spades_folder"/pipeline_state/stage_7_terminate
rm "$spades_folder"/pipeline_state/stage_7_k55
rm "$spades_folder"/pipeline_state/stage_8_copy_files
rm "$spades_folder"/pipeline_state/stage_9_as_finish


echo "Do you wish to continue the spades pipeline?
with: $spades_py_executable --continue -o $1 (y/n)"
read continue_pipeline
if [ "$continue_pipeline" == "y" ]; then
"$spades_py_executable" --continue -o "$1"
else
echo "done"
fi
